package ai_lab_first;

import com.bulenkov.darcula.DarculaLaf;
import org.jfree.chart.*;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.*;
import java.util.List;

public class AppGUI extends JFrame {
    private static JFreeChart chart;
    private static ChartPanel chartPanel;
    private static Boolean alternateAlgo = false;
    private static Classifier cl;
    private static String filename;
    private final static Color darculaGREY = new Color(60, 63, 65);
    private final static Color darculaLIGHT = new Color(164, 164, 165);

    public AppGUI( String title ) {
        super( title );

        try {
            UIManager.setLookAndFeel(DarculaLaf.class.getName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        if ( AppGUI.filename == null ) {
            openFileDialog();
        }
        // Generate
        AppGUI.cl = new Classifier(Classifier.readSeriesFromCSV( AppGUI.filename ), new ArrayList<>());
        try {
            cl.classifyByClosest();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        // Draw UI
        setContentPane(AppGUI.initUI( cl ));
    }

    public static JPanel initUI( Classifier cl ) {
        // Add components
        //createDemoPanel(cl.getSeries()))
        JPanel rootPanel = new JPanel();
        rootPanel.setLayout(new BorderLayout());
        rootPanel.setBorder(BorderFactory.createEmptyBorder(5, 15, 15, 15));

        JRadioButton algoBtnOne = new JRadioButton("Ближайший сосед");
        JRadioButton algoBtnTwo = new JRadioButton("K ближайших соседей");
        algoBtnOne.setBorder(BorderFactory.createEmptyBorder(5, 15, 10, 10));
        algoBtnTwo.setBorder(BorderFactory.createEmptyBorder(5, 15, 10, 10));
        // Create group
        ButtonGroup radioGroup = new ButtonGroup();
        radioGroup.add(algoBtnOne);
        radioGroup.add(algoBtnTwo);
        // Add to panel
        rootPanel.add(algoBtnOne, BorderLayout.WEST);
        rootPanel.add(algoBtnTwo, BorderLayout.CENTER);
        algoBtnOne.setSelected(true); // Default algorithm
        if ( AppGUI.alternateAlgo ) {
            algoBtnTwo.setSelected(true);
        }
        // Set listeners
        algoBtnOne.addActionListener( e -> { AppGUI.alternateAlgo = false; AppGUI.redrawFrame( false, true ); });
        algoBtnTwo.addActionListener( e -> { AppGUI.alternateAlgo = true; AppGUI.redrawFrame( false, true ); });

        JButton resetBtn = new JButton("Сбросить");
        resetBtn.addActionListener( e -> redrawFrame( true, false ));
        rootPanel.add(resetBtn, BorderLayout.EAST);

        JButton openBtn = new JButton("Выбрать файл");
        openBtn.addActionListener( e -> AppGUI.openFileDialog() );
        rootPanel.add(openBtn, BorderLayout.NORTH);

        rootPanel.add(createDemoPanel(cl.getSeries()), BorderLayout.SOUTH);

        return rootPanel;
    }

    public static JPanel createDemoPanel( Map<Integer, Seria> seriesMap ) {
        AppGUI.chart = createChart(createDataset( seriesMap ));
        AppGUI.chartPanel = new ChartPanel( chart );
        // Add events to mouse click
        AppGUI.chartPanel.addChartMouseListener(new ChartMouseListener() {

            public void chartMouseClicked(ChartMouseEvent e) {
                MouseEvent event =  e.getTrigger();
                switch( event.getButton() ) {
                    case MouseEvent.BUTTON1:
                        AppGUI.redrawFrame( false, false );
                        break;
                }
            }

            public void chartMouseMoved(ChartMouseEvent e) {}
        });

        return AppGUI.chartPanel;
    }

    private static JFreeChart createChart(XYDataset dataset) {
        JFreeChart chart_xy = ChartFactory.createScatterPlot(
                "",
                "Признак X",
                "Признак Y",
                dataset);

        // Font styling
        final String fontFamily = "Arial";
        final StandardChartTheme chartTheme = (StandardChartTheme)StandardChartTheme.createJFreeTheme();

        final Font oldExtraLargeFont = chartTheme.getExtraLargeFont();
        final Font oldLargeFont = chartTheme.getLargeFont();
        final Font oldRegularFont = chartTheme.getRegularFont();
        final Font oldSmallFont = chartTheme.getSmallFont();

        final Font extraLargeFont = new Font(fontFamily, oldExtraLargeFont.getStyle(), oldExtraLargeFont.getSize());
        final Font largeFont = new Font(fontFamily, oldLargeFont.getStyle(), oldLargeFont.getSize());
        final Font regularFont = new Font(fontFamily, oldRegularFont.getStyle(), oldRegularFont.getSize());
        final Font smallFont = new Font(fontFamily, oldSmallFont.getStyle(), oldSmallFont.getSize());

        chartTheme.setExtraLargeFont(extraLargeFont);
        chartTheme.setLargeFont(largeFont);
        chartTheme.setRegularFont(regularFont);
        chartTheme.setSmallFont(smallFont);

        chartTheme.setLegendBackgroundPaint(darculaGREY);
        chartTheme.setLegendItemPaint(darculaLIGHT);
        chartTheme.setChartBackgroundPaint(darculaGREY);
        chartTheme.setAxisLabelPaint(darculaLIGHT);
        chartTheme.setTickLabelPaint(darculaLIGHT);

        chartTheme.apply(chart_xy);
        chart_xy.setAntiAlias(true);
        chart_xy.getLegend().setFrame(BlockBorder.NONE);

        // Plot styling
        final XYPlot plot = chart_xy.getXYPlot();
        XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        // Remove lines visibility
        for (int i = 0; i < dataset.getSeriesCount(); i++) {
            renderer.setSeriesLinesVisible(i, false);
        }
        // Background
        plot.setBackgroundPaint(AppGUI.darculaGREY);
        plot.setRangeGridlinePaint(Color.WHITE);
        plot.setDomainGridlinePaint(Color.WHITE);

        plot.setRenderer(renderer);

        // Set range
        ValueAxis yAxis = plot.getRangeAxis();
        yAxis.setAutoRange(false);
        yAxis.setRange(0, 100);

        ValueAxis xAxis = plot.getDomainAxis();
        xAxis.setAutoRange(false);
        xAxis.setRange(0, 100);

        return chart_xy;
    }

    private static XYDataset createDataset( Map<Integer, Seria> seriesMap ) {
        final XYSeriesCollection dataset = new XYSeriesCollection();
        XYSeries xySeria;
        for (Map.Entry<Integer, Seria> s : seriesMap.entrySet()) {
            String name;
            if (s.getValue().getPoints().get(0).getSeriaClass() == 0) {
                name = "Неклассифицированные";
            } else {
                name = "Класс " + s.getValue().getPoints().get(0).getSeriaClass();
            }

            xySeria = new XYSeries( name );
            for (Point p : s.getValue().getPoints()) {
                xySeria.add(p.getX(), p.getY());
            }

            dataset.addSeries(xySeria);
        }

        return dataset;
    }

    private static void redrawFrame( Boolean reset, Boolean useOldSet ) {

        List<Point> pointsToRecognize = new ArrayList<>();
        // Check params
        if ( !reset ) {
            if ( !useOldSet ) {
                pointsToRecognize = Classifier.generateRandomSeria(32);
            } else {
                pointsToRecognize = AppGUI.cl.getUnknownSeria();
            }
        }

        AppGUI.resetUI( pointsToRecognize, reset );
    }

    private static void resetUI( List<Point> pointsToRecognize, Boolean reset ) {
        JFrame appFrame = (JFrame) AppGUI.getFrames()[0];
        // Init classifier
        try {
            AppGUI.cl = new Classifier(Classifier.readSeriesFromCSV( AppGUI.filename ), pointsToRecognize);
            if ( !reset ) {
                if ( !AppGUI.alternateAlgo ) {
                    cl.classifyByClosest();
                } else {
                    cl.classifyByKNeighbors( 3 );
                }
            }
            // Update frame content
            appFrame.setContentPane(AppGUI.initUI( cl ));
            appFrame.setVisible(true);
            appFrame.pack();
            appFrame.repaint();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static void openFileDialog() {
        JFileChooser fileopen = new JFileChooser(
                AppGUI.filename == null ? System.getProperty("user.dir") : AppGUI.filename
        );
        int ret = fileopen.showDialog(null, "Открыть файл");
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileopen.getSelectedFile();
            AppGUI.filename = file.getAbsolutePath();
            AppGUI.redrawFrame( false, false );
        } else {
            if ( AppGUI.filename == null ) {
                System.exit(1);
            }
        }
    }
}
