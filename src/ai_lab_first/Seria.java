package ai_lab_first;

import java.util.ArrayList;
import java.util.List;

public class Seria {
	private List<Point> points;
		
	public Seria() {
		this.points = new ArrayList<Point>();
	}
			
	public void addPoint(Point p) {
		this.points.add(p);
	}
	
	public void clearSeria() {
		this.points.removeAll(this.points);
	}
	
	public void removePoint(Point p) {
		this.points.remove(p);
	}
	
	public List<Point> getPoints() {
		return this.points;
	}
}
