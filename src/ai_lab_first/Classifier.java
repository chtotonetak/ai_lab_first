package ai_lab_first;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Classifier {
    private List<Point> referenceSeries;
    private List<Point> unknownSeria;
    private Map<Integer, Seria> recognitedSeries;

    public Classifier(List<Point> seriaList, List<Point> unknownSeria) {
        this.referenceSeries = seriaList;
        this.unknownSeria = unknownSeria;
        this.recognitedSeries = new HashMap<>();
    }

    public List<Point> getUnknownSeria() {
        return this.unknownSeria;
    }

    public void classifyByClosest() throws Exception {
        // Check errors
        if (this.unknownSeria.isEmpty()) {
            throw new Exception("Нет точек для обработки");
        }
        // Haven't reference series of points
        if (this.referenceSeries.isEmpty()) {
            throw new Exception("Нет обучающей выборки");
        }
        // Search algorithm
        for (Point p : this.unknownSeria) {
            // Find class by length
            Double min_length = null;
            for (Point ref_p : this.referenceSeries) {
                Double length = p.getLength(ref_p);
                if (min_length == null || min_length > length) {
                    min_length = length;
                    p.setSeriaClass(ref_p.getSeriaClass());
                }
            }
        }
    }

    public void classifyByKNeighbors( Integer count ) throws Exception {
        // Check errors
        if (this.unknownSeria.isEmpty()) {
            throw new Exception("Нет точек для обработки");
        }
        // Haven't reference series of points
        if (this.referenceSeries.isEmpty()) {
            throw new Exception("Нет обучающей выборки");
        }
        // Add all points to set
        this.unknownSeria.addAll(this.referenceSeries);

        this.classifyByKNeighborsIteration( count );
    }

    private void classifyByKNeighborsIteration( Integer nCount ) {
        boolean hasChanges = false;
        // Search algorithm
        for (Point p : this.unknownSeria) {
            int prevClass = p.getSeriaClass();

            Map<Double, Point> pointMap = new HashMap<>();
            // Recompute training set
            for (Point ref_p : this.unknownSeria) {
                if ( ref_p.getSeriaClass() != 0 ) {
                    pointMap.put( ref_p.getLength( p ), ref_p );
                }
            }
            // Sort array of lengths between points
            SortedSet<Double> keys = new TreeSet<>(pointMap.keySet());
            // Iterate through k neighbors
            Integer currNeigh = 0;
            List<Integer> neighClasses = new ArrayList<>();
            for (Double lengthToNeighbour : keys) {
                if (currNeigh < nCount) {
                    if ( lengthToNeighbour <= 25.0 ) {
                        neighClasses.add(pointMap.get(lengthToNeighbour).getSeriaClass());
                        currNeigh++;
                    }
                } else {
                    break;
                }
            }
            // Define point class
            if ( !neighClasses.isEmpty() ) {
                // Save state
                int oldClass = p.getSeriaClass();
                // Find most common class
                p.setSeriaClass(mostCommonNumberInArray(neighClasses));
                // Check changes
                if ( oldClass != p.getSeriaClass() ) {
                    hasChanges = true;
                }
            }
        }
        // Next iteration
        if ( hasChanges ) {
            this.classifyByKNeighborsIteration( nCount );
        }
    }

    public static Integer mostCommonNumberInArray( List<Integer> arr ) {
        Map<Integer, Integer> map = new HashMap<>();
        for (Integer i : arr) {
            Integer count = map.get(i);
            map.put(i, count != null ? count + 1 : 0);
        }

        return Collections.max(map.entrySet(),
                (Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) -> o1.getValue().compareTo(o2.getValue())
        ).getKey();
    }

    // Generate HashMap with key by seria class
    private void generateSeries() {
        // Clear old output
        this.recognitedSeries = new HashMap<>();
        // Sort points from previously unknown seria
        this.pointsToSeria(this.unknownSeria);
        // Sort from reference
        this.pointsToSeria(this.referenceSeries);
    }

    private void pointsToSeria( List<Point> points ) {
        for (Point p : points) {
            if (this.recognitedSeries.get(p.getSeriaClass()) == null) {
                this.recognitedSeries.put(p.getSeriaClass(), new Seria());
            }

            this.recognitedSeries.get(p.getSeriaClass()).addPoint(p);
        }
    }

    public Map<Integer, Seria> getSeries() {
        // Generate series from points
        this.generateSeries();

        return this.recognitedSeries;
    }

    public static List<Point> generateRandomSeria( int size ) {
        List<Point> unknownSeria = new ArrayList<>();
        final Random rnd = new Random();
        for (int i = 0; i < size; i++) {
            unknownSeria.add(new Point(rnd.nextInt(100), rnd.nextInt(100)));
        }

        return unknownSeria;
    }

    public static List<Point> readSeriesFromCSV( String csvFile ) {
        List<Point> seriaList = new ArrayList<>();
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ";";
        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] data = line.split(cvsSplitBy);
                seriaList.add(new Point(Integer.parseInt(data[0]), Integer.parseInt(data[1]), Integer.parseInt(data[2])));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return seriaList;
    }

}
