package ai_lab_first;

public class Point {
    private Double x;
    private Double y;
    private int seriaClass = 0;

    public Point(Double x, Double y) {
        this.x = x;
        this.y = y;
    }

    public Point(int x, int y) {
        this.x = (double)x;
        this.y = (double)y;
    }

    public Point(int x, int y, int seriaClass) {
        this(x, y);
        this.seriaClass = seriaClass;
    }

    public Double getX() {
        return this.x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public void setX(int x) {
        this.x = (double)x;
    }

    public Double getY() {
        return this.y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public void setY(int y) {
        this.y = (double)y;
    }

    public Integer getSeriaClass() {
        return this.seriaClass;
    }

    public void setSeriaClass(int c) {
        this.seriaClass = c;
    }

    public Double getLength(Point p) {
        return Math.sqrt(
                Math.pow((this.x - p.getX()), 2.0) +
                        Math.pow((this.y - p.getY()), 2.0));
    }
}
